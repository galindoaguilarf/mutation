# Calcular DNA
Rest API que calcula si una secuencia DNA tiene o no mutación

### Pre-requirements 📋
- Tener instalado cliente de git como por ejemplo GitBash https://gitforwindows.org/
- Tener instalado la versión 11 de java https://adoptopenjdk.net

## Deploy 📦
- Abrimos una terminal
- Clonamos el proyecto con el comando git clone https://gitlab.com/galindoaguilarf/mutation.git
- Ahora Ingresamos a la carpeta con el comando cd mutation
- Ejecutamos el siguiente comando para linux ./gradlew build o para windows gradlew build 
- Ejecutamos el siguiente comando java -jar build/libs/mutation-1.0.jar
- Con esto ya tendremos desplegado la API desplegada en local

## Ejecución 📦
- Es necesario tener instalado un cliente para consumir servicios rest como https://www.postman.com/ o similar
- La url para acceder a los servicios será local http:localhost:8080/guros/api/ y productiva http:ec2-18-220-22-55.us-east-2.compute.amazonaws.com:8080/guros/api/
- Los servicios expuesto se consumen de la siguiente manera:
  - Productiva : http://ec2-18-220-22-55.us-east-2.compute.amazonaws.com:8080/guros/api/mutation/
  - Local : http://localhost:8080/guros/api/mutation/  
  - POST
    BODY
    {
        "dna":["AAGCGA", "AAGTGC", "TTATGT", "AGAAGG", "CCCBTA","TCACTG"]
    }
    
  - GET
  - Productiva : http://ec2-18-220-22-55.us-east-2.compute.amazonaws.com:8080/guros/api/stats/
  - Local : http://localhost:8080/guros/api/stats/

## Build with 🛠️
- JAVA
- Gradle